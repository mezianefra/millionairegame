package com.meziane.millionairegame.datastore

import kotlinx.coroutines.flow.Flow

interface DataPref {
    suspend fun setToken(value: String)
    suspend fun getToken(): Flow<String>
    suspend fun deleteToken()
}