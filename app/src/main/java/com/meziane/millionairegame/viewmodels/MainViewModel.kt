package com.meziane.millionairegame.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.meziane.millionairegame.model.Repo
import com.meziane.millionairegame.models.Data
import com.meziane.millionairegame.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor ( val repo: Repo) : ViewModel() {
    private var _state: MutableLiveData<Resource<Data>> = MutableLiveData(Resource.Loading())
    val state: LiveData<Resource<Data>> get() = _state
    private var _username: MutableLiveData<String> = MutableLiveData()
    val username: LiveData<String> get() = _username



    init {
        getData()
        getUsername()
    }

    private fun getUsername() = viewModelScope.launch(Dispatchers.Main) {
        _username.value = repo.getUser()
    }

    private fun getData() = viewModelScope.launch(Dispatchers.Main) {
        _state.value = repo.getData()
    }

    fun setUser(user: String) {
        viewModelScope.launch(Dispatchers.Main) {
            Log.d("user", user)
            repo.saveUser(user)
        }
//        fun getUsername() = viewModelScope.launch(Dispatchers.Main) {
//        _username.value= repo.getUser()
        }



    }
