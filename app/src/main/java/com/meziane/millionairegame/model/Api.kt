package com.meziane.millionairegame.model

import androidx.room.Dao
import com.google.gson.annotations.SerializedName
import com.meziane.millionairegame.models.Data
import com.meziane.millionairegame.models.Result
import retrofit2.Response
import retrofit2.http.GET

@Dao
interface Api {

    @GET("api.php?amount=15&type=multiple")
     suspend fun getData(): Response<Data>
}