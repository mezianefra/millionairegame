package com.meziane.millionairegame.model

import android.util.Log
import com.meziane.millionairegame.datastore.DataPref
import com.meziane.millionairegame.model.Api
import com.meziane.millionairegame.models.Data
import com.meziane.millionairegame.room.GameTable
import com.meziane.millionairegame.room.WtmDatabase
import com.meziane.millionairegame.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.withContext
import javax.inject.Inject

class Repo @Inject constructor(
    val api: Api,
    val wtmDatabase: WtmDatabase,
    val dataPrefSource: DataPref
) {
    private var username: String? = null
    private val myTable = wtmDatabase.tableQueries()
    suspend fun getData() : Resource<Data>? = withContext(Dispatchers.IO){
        return@withContext try {
            val response = api.getData()
            val db_response = myTable.getTableData()
            if (!response.isSuccessful || (response.body())!!.results!!.size <= db_response.size) {
                val questions = response.body()!!.results!!.map {
                    GameTable(

                        category = it.category!!,
                        correct_answer = it.correct_answer!!,
                        difficulty = it.difficulty!!,
                        incorrect_answers = it.incorrect_answers!!,
                        question = it.question!!,
                        type = it.type!!,

                        )

                }
                myTable.addrow(questions)
            }
            Resource.Success(response.body()!!.results)

        }catch (e: Exception){
            e.localizedMessage?.let { Resource.Error(it) }
        }
    }
     suspend fun saveUser(user: String) = withContext(Dispatchers.IO){
         return@withContext try {
             dataPrefSource.setToken(user)
            Log.d("USER", username!!)
         }
         catch (e: Exception){
             Log.d("error","date pref error")
         }
    }
    suspend fun getUser() = withContext(Dispatchers.IO){
            username = dataPrefSource.getToken().firstOrNull() ?: ""
        return@withContext username
    }
    suspend fun deleteUser(){
        dataPrefSource.deleteToken()
    }
}