package com.meziane.millionairegame.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.meziane.millionairegame.util.Converters


@Database(entities = [GameTable::class], exportSchema =false, version = 9 )
@TypeConverters(Converters::class)

abstract class WtmDatabase: RoomDatabase() {

    abstract fun tableQueries(): TableQueries
}