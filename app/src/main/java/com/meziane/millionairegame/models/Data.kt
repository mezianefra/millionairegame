package com.meziane.millionairegame.models

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Data(
    val vresponse_code: Int?,
    val results: List<Result>?
):Parcelable