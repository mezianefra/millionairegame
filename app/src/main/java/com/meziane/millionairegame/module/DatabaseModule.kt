package com.meziane.millionairegame.module

import android.content.Context
import androidx.room.Room
import com.meziane.millionairegame.room.WtmDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    const val DB_NAME = "dataTable"
    @Provides
    @Singleton
    fun getDatabaseInstance(@ApplicationContext context: Context) : WtmDatabase {
        return Room.databaseBuilder(context, WtmDatabase::class.java, DB_NAME)
            .fallbackToDestructiveMigration().build()
    }
}