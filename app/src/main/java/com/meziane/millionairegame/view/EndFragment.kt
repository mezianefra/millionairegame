package com.meziane.millionairegame.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.meziane.millionairegame.databinding.FragmentEndBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EndFragment : Fragment() {
    private var _binding: FragmentEndBinding? = null
    private val binding: FragmentEndBinding get() = _binding!!
    val args: EndFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentEndBinding.inflate(inflater, container, false ).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        binding.tvScore.text = args.score.toString()
OnBackPressedCallback()  }

    private fun OnBackPressedCallback() {
Log.d("end","end")    }
}