package com.meziane.millionairegame.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.meziane.millionairegame.databinding.FragmentJokersBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class JokersFragment : Fragment() {
    private var _binding: FragmentJokersBinding? = null
    private val binding: FragmentJokersBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentJokersBinding.inflate(inflater, container, false ).also {
        _binding = it
    }.root

}