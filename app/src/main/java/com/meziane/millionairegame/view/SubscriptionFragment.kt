package com.meziane.millionairegame.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import com.meziane.millionairegame.databinding.FragmentSubscriptionBinding
import com.meziane.millionairegame.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SubscriptionFragment : Fragment() {

    private var _binding: FragmentSubscriptionBinding? = null
    private val binding: FragmentSubscriptionBinding get() = _binding!!
    private val viewmodel by viewModels<MainViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSubscriptionBinding.inflate(inflater, container, false ).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()

    }

    private fun initListeners() = with(binding) {
        viewmodel.username.observe(viewLifecycleOwner){ user ->
            if (!user.isNullOrBlank()){
                tvUsername.setText(user)
                val action =
                    SubscriptionFragmentDirections.actionSubscriptionFragmentToQuestionsFragment()
                findNavController().navigate(action)
            }

        }
        start.setOnClickListener{
            if (binding.tvUsername.text.isNullOrBlank()){
                tiUsername.hint ="Field cannot be empty"
            }
            else {
              viewmodel.setUser(tvUsername.text.toString())
                val action =
                    SubscriptionFragmentDirections.actionSubscriptionFragmentToQuestionsFragment()
                findNavController().navigate(action)
            }
        }
    }

}