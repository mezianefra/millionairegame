package com.meziane.millionairegame.view

import android.graphics.Color
import android.media.MediaPlayer
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.meziane.millionairegame.databinding.FragmentQuestionsBinding
import com.meziane.millionairegame.util.Resource
import com.meziane.millionairegame.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import kotlin.concurrent.schedule


@AndroidEntryPoint
class QuestionsFragment : Fragment() {


    private var _binding: FragmentQuestionsBinding? = null
    private val binding: FragmentQuestionsBinding get() = _binding!!
    private val viewModel by viewModels<MainViewModel>()
    private var score = 0
    private var responseSelected = 0
    private var questionNumber = 0
    private var correct =0
    private var fifty = 1


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentQuestionsBinding.inflate(inflater, container, false ).also {
        _binding = it

//        music.stop()
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            initListners()
}

    private fun initListners() = with(binding) {
        viewModel.state.observe(viewLifecycleOwner) { viewState ->
            when(viewState) {
                is Resource.Error ->{
                    Log.d("Eerr","errrorr")
                }
                is Resource.Loading ->{
                    Log.d("load","Load")
                }
                is Resource.Success ->{
                    fun nextQuestion() {
                        imageView.visibility = View.VISIBLE
                        audience.visibility = View.GONE
                        tvResponse1.visibility = View.VISIBLE
                        tvResponse2.visibility = View.VISIBLE
                        tvResponse3.visibility = View.VISIBLE
                        tvResponse4.visibility = View.VISIBLE
                        tvResponse1.setBackgroundColor(Color.parseColor("#030C25"))
                        tvResponse2.setBackgroundColor(Color.parseColor("#030C25"))
                        tvResponse3.setBackgroundColor(Color.parseColor("#030C25"))
                        tvResponse4.setBackgroundColor(Color.parseColor("#030C25"))
                        tvResponse1.setTextColor(Color.WHITE)
                        tvResponse2.setTextColor(Color.WHITE)
                        tvResponse3.setTextColor(Color.WHITE)
                        tvResponse4.setTextColor(Color.WHITE)

                        val randomList = (0..3).shuffled().take(4)
                        val response = arrayOf(
                            viewState.data?.get(questionNumber)?.correct_answer?: "",
                            viewState.data?.get(questionNumber)?.incorrect_answers?.get(0) ?: "",
                            viewState.data?.get(questionNumber)?.incorrect_answers?.get(1) ?: "",
                            viewState.data?.get(questionNumber)?.incorrect_answers?.get(2) ?: ""
                        )

                        tvQuestion.text = viewState.data?.get(questionNumber)?.question
                        tvResponse1.text = response[randomList[0]]
                        tvResponse2.text = response[randomList[1]]
                        tvResponse3.text = response[randomList[2]]
                        tvResponse4.text = response[randomList[3]]

                        lyButtons.children.forEach { button ->

                            button.setOnClickListener { view->
                                tvResponse1.setBackgroundColor(Color.parseColor("#030C25"))
                                tvResponse2.setBackgroundColor(Color.parseColor("#030C25"))
                                tvResponse3.setBackgroundColor(Color.parseColor("#030C25"))
                                tvResponse4.setBackgroundColor(Color.parseColor("#030C25"))
                                tvResponse1.setTextColor(Color.WHITE)
                                tvResponse2.setTextColor(Color.WHITE)
                                tvResponse3.setTextColor(Color.WHITE)
                                tvResponse4.setTextColor(Color.WHITE)
                                when(view.id){
                                    tvResponse1.id ->{
                                        tvResponse1.setBackgroundColor(Color.YELLOW)
                                        tvResponse1.setTextColor(Color.BLACK)
                                    }
                                    tvResponse2.id ->{
                                        tvResponse2.setBackgroundColor(Color.YELLOW)
                                        tvResponse2.setTextColor(Color.BLACK)
                                    }
                                    tvResponse3.id ->{
                                        tvResponse3.setBackgroundColor(Color.YELLOW)
                                        tvResponse3.setTextColor(Color.BLACK)
                                    }
                                    tvResponse4.id ->{
                                        tvResponse4.setBackgroundColor(Color.YELLOW)
                                        tvResponse4.setTextColor(Color.BLACK)
                                }

                                }
//                                tvResponse1.setBackgroundColor(Color.parseColor("#030C25"))
//                                tvResponse2.setBackgroundColor(Color.parseColor("#030C25"))
//                                tvResponse3.setBackgroundColor(Color.parseColor("#030C25"))
//                                tvResponse4.setBackgroundColor(Color.parseColor("#030C25"))
//                                it.setBackgroundColor(Color.YELLOW)
//                                Log.d("color",tvResponse1.background.current.toString())
                                responseSelected = view.id
                                Log.d("responseSelected",responseSelected.toString())


                            }
                        }

                    }
                    nextQuestion()

                    binding.jokers.fifty.setOnClickListener {
                        binding.jokers.fifty.visibility = View.INVISIBLE
                        if (binding.tvResponse1.text != viewState.data?.get(questionNumber)?.correct_answer
                            && fifty<3){
                            tvResponse1.visibility=View.INVISIBLE
                            fifty+=1

                        }
                        if (binding.tvResponse2.text != viewState.data?.get(questionNumber)?.correct_answer
                            && fifty<3){
                            tvResponse2.visibility=View.INVISIBLE
                            fifty+=1
                        }
                        if (binding.tvResponse3.text != viewState.data?.get(questionNumber)?.correct_answer
                            && fifty<3){
                            tvResponse3.visibility=View.INVISIBLE
                            fifty+=1
                        }
                        if (binding.tvResponse4.text != viewState.data?.get(questionNumber)?.correct_answer
                            && fifty<3){
                            tvResponse4.visibility=View.INVISIBLE
                            fifty+=1
                        }

                    }

                    binding.jokers.audience.setOnClickListener {
                        binding.jokers.audience.visibility = View.INVISIBLE
                        imageView.visibility = View.GONE
                        audience.visibility = View.VISIBLE
                        callResponse.visibility = View.GONE
                        a.visibility = View.VISIBLE
                        b.visibility = View.VISIBLE
                        c.visibility = View.VISIBLE
                        d.visibility = View.VISIBLE
                        if (binding.tvResponse1.text == viewState.data?.get(questionNumber)?.correct_answer
                            && tvResponse1.visibility == View.VISIBLE){
                            a.text = "A- 52%"
                        }
                        else if (tvResponse1.visibility == View.VISIBLE){ a.text = "A- 16%"}
                        if (binding.tvResponse2.text == viewState.data?.get(questionNumber)?.correct_answer
                            && tvResponse2.visibility == View.VISIBLE){
                            b.text = "B- 52%"
                        }
                        else if (tvResponse2.visibility == View.VISIBLE){ b.text = "B- 16%"}
                        if (binding.tvResponse3.text == viewState.data?.get(questionNumber)?.correct_answer
                            && tvResponse3.visibility == View.VISIBLE){
                            c.text = "C- 52%"
                        }
                        else if (tvResponse3.visibility == View.VISIBLE){ c.text = "C- 16 %"}
                        if (binding.tvResponse4.text == viewState.data?.get(questionNumber)?.correct_answer
                            && tvResponse4.visibility == View.VISIBLE){
                            d.text = "D- 52%"
                        }
                        else if (tvResponse4.visibility == View.VISIBLE){ d.text = "D- 16%"}
                    }
                    binding.jokers.call.setOnClickListener {
                        binding.jokers.call.visibility = View.INVISIBLE
                        imageView.visibility = View.GONE
                        audience.visibility = View.VISIBLE
                        callResponse.visibility = View.VISIBLE
                        a.visibility = View.GONE
                        b.visibility = View.GONE
                        c.visibility = View.GONE
                        d.visibility = View.GONE

                        callResponse.text = "Hi, I think the answer is "+ (viewState.data?.get(questionNumber)?.correct_answer
                            )
                    }
                    btSubmit.setOnClickListener {

                        if (tvResponse1.text.toString() == viewState.data?.get(questionNumber)?.correct_answer.toString()
                                && tvResponse1.id.toString() == responseSelected.toString()
                            ) {
                                score+=10
                            }

                            if (tvResponse2.text.toString() == viewState.data?.get(questionNumber)?.correct_answer.toString()
                                && tvResponse2.id.toString() == responseSelected.toString()
                            ) {
                                score += 10
                            }

                            if (tvResponse3.text.toString() == viewState.data?.get(questionNumber)?.correct_answer.toString()
                                && tvResponse3.id.toString() == responseSelected.toString()
                            ) {
                                score += 10
                            }


                            if (tvResponse4.text.toString() == viewState.data?.get(questionNumber)?.correct_answer.toString()
                                && tvResponse4.id.toString() == responseSelected.toString()
                            ) {
                                score += 10
                            }
                            questionNumber += 1
                            Log.d("score", score.toString())
                            if (score <(questionNumber*10)){
                                val action = QuestionsFragmentDirections.actionQuestionsFragmentToEndFragment(score)
                                findNavController().navigate(action)}



                            if (questionNumber < 10) {


                                nextQuestion()

                            }
                            else{
                            val action = QuestionsFragmentDirections.actionQuestionsFragmentToEndFragment(score)
                            findNavController().navigate(action)}
                    }
                    }

                }

            }

        }


    override fun onResume() {
        super.onResume()
        val music: MediaPlayer = MediaPlayer.create(context,com.meziane.millionairegame.R.raw.letsplay)
        music.start()
        if (music.isPlaying){
            music.setOnCompletionListener {
                music.release()
            }
        }



    }
}



