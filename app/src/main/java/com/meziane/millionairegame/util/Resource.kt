package com.meziane.millionairegame.util

import com.meziane.millionairegame.models.Data
import com.meziane.millionairegame.models.Result


sealed class Resource<T>(data: Any? = null, message: String?){
    data class Success<T>(val data: List<Result>?): Resource<T>(data,null)
    class Loading<T> : Resource<T>(null,null)
    data class Error<T>(val message: String): Resource<T>(null,message)

}